import idaapi
import pprint
from struct import *
from idaapi import *

# sign extend b low bits in x
# from "Bit Twiddling Hacks"
def SIGNEXT(x, b):
  m = 1 << (b - 1)
  x = x & ((1 << b) - 1)
  return (x ^ m) - m

def SIGNEXT_32bits(x, b): # Sign Extend <b> bits of x to 32-bits. Will return Signed INT.
    extended_result=SIGNEXT(x,b) & 0b11111111111111111111111111111111
    temp_value= struct.pack('I',extended_result)
    temp_value= struct.unpack('i',temp_value)
    return temp_value[0]

class mep_processor_t(idaapi.processor_t):

    def init_instructions(self):
      class idef:
        """
        Internal class that describes an instruction by:
        - instruction id
        - instruction name
        - canonical flags used by IDA
        - conditional modifier instruction indicator
        - instruction comment
        """
        def __init__(self, op, name, cf, con = False, cmt = None):
          self.op = op
          self.name = name
          self.cf  = cf
          self.con = con
          self.cmt = cmt
          self.itype = None

        def ToDict(self):
          new_dict = {'op': self.op, 'name': self.name, 'feature': self.cf}
          if self.cmt:
            new_dict['cmt'] = self.cmt
          return new_dict

        def IsNonBasic(self):
          return not (self.op & 0xFF00)

      # ======================================================================
      # Instructions table (w/ pointer to decoder)
      # in the form <OP_Code>, <Menemonics*>, <Canonical_Feature*>,<Comment*>
      # Only those marked * are required by IDA. This script use the <OP_Code> as a conveinence to keep track internally.
      # Since MeP opcode is at bits level not byte, we will use the convention 0x0[Major][Sub][Minor]
      # ======================================================================
      # writing to Memory locations EG Mem(R1) <- R1 is not considered a CF_USE flag

      self.itable = [
          #### Special OPCODE for Internal USE ####
          idef(0xabad1dea, '0xabad1dea',   0,     cmt='Bad Instructions!'),

          #### MAJOR OPCODE 0 ####
          idef(0x0000, 'MOV',   CF_CHG1|CF_USE2,     cmt='Mov Op1 <- Op2'),
          idef(0x0001, 'NEG',   CF_CHG1|CF_USE2,           cmt='Does a PUSH of PC and jumps to A'),
          idef(0x0002, 'SLT3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='if (Rn<Rm) R0<-1 else R0<-0 (Signed)'),
          idef(0x0003, 'SLTU3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='if (Rn<Rm) R0<-1 else R0<-0 (UnSigned)'),
          idef(0x0004, 'SUB',   CF_CHG1|CF_USE2,     cmt=''),
          idef(0x0005, 'SBVCK3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='if(Overflow(Rn-Rm)) R0<-1 else R0<-0 (Signed)'),
          idef(0x0006, 'RESERVE',   0,     cmt='RESERVED OP CODE'),
          idef(0x0007, 'ADVCK3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='if(Overflow(Rn-Rm)) R0<-1 else R0<-0 (Signed)'),
          idef(0x0008, 'SB',   CF_USE1|CF_USE2,     cmt=' MemByte((SignExt(disp16)+Rm)31..0) <- Rn7..0'),
          idef(0x0009, 'SH',   CF_USE1|CF_USE2,     cmt='MemHword((SignExt(disp16)+Rm)31..1||0) <- Rn15..0'),
          idef(0x000A, 'SW',   CF_USE1|CF_USE2,     cmt='MemHword((SignExt(disp16)+Rm)31..1||0) <- Rn15..0'),
          idef(0x000B, 'LBU',   CF_CHG1|CF_USE2,     cmt='Rn <- ZeroExt(MemByte(Rm31..0))'),
          idef(0x000C, 'LB',   CF_CHG1|CF_USE2,     cmt='Rn <- SignExt(MemByte(Rm31..0))'),
          idef(0x000D, 'LH',   CF_CHG1|CF_USE2,     cmt='Rn <- SignExt(MemHword(Rm31..1||0))'),
          idef(0x000E, 'LW',   CF_CHG1|CF_USE2,     cmt='Rn <- MemWord(Rm31..2||00)'),
          idef(0x000F, 'LHU',   CF_CHG1|CF_USE2,     cmt='Rn <- ZeroExt(MemHword(Rm31..1||0))'),

          #### MAJOR OPCODE 1 ####
          idef(0x0100, 'OR',   CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- Rn or Rm'),
          idef(0x0101, 'AND',  CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- Rn and Rm'),
          idef(0x0102, 'XOR',  CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- Rn xor Rm'),
          idef(0x0103, 'NOR',  CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- Rn nor Rm'),
          idef(0x0104, 'MUL',  CF_USE1|CF_USE2,             cmt='HI||LO <- Rn * Rm (Signed)'),
          idef(0x0105, 'MULU', CF_USE1|CF_USE2,            cmt='HI||LO <- Rn * Rm (Unsigned)'),
          idef(0x0106, 'MULR',  CF_USE1|CF_USE2,           cmt='HI||LO <- Rn * Rm; Rn <- LO (Signed)'),
          idef(0x0107, 'MULRU', CF_USE1|CF_USE2,        cmt='Mov Op1 <- Op2'),
          idef(0x0108, 'DIV', CF_USE1|CF_USE2,        cmt='LO <- Rn / Rm, HI <- Rn % Rm (Signed)'),
          idef(0x0109, 'DIVU',  CF_USE1|CF_USE2,      cmt='LO <- Rn / Rm, HI <- Rn % Rm (Unsigned)'),
          idef(0x010A, 'RESERVE_1_10',   0,     cmt='RESERVED OP CODE'),
          idef(0x010B, 'RESERVE_1_11',   0,     cmt='RESERVED OP CODE'),
          idef(0x010C, 'SSARB',   CF_USE1,     cmt='SAR <- ZeroExt((disp2+Rm)1..0 *8) where dd = disp2'),
          idef(0x010D, 'EXTB',  CF_CHG1|CF_USE1,     cmt='Rn <- SignExt(Rn7..0)'),
          idef(0x012D, 'EXTH',  CF_CHG1|CF_USE1,     cmt='Rn <- SignExt(Rn15..0)'),
          idef(0x018D, 'EXTUB',  CF_CHG1|CF_USE1,     cmt='Rn <- ZeroExt(Rn7..0)'),
          idef(0x01AD, 'EXTUH',  CF_CHG1|CF_USE1,     cmt='Rn <- ZeroExt(Rn15..0)'),
          idef(0x010E, 'JMP',  CF_USE1|CF_JUMP|CF_STOP,     cmt='PC <- Rm31..1||0  [** read VLIW]'),
          idef(0x010F, 'JSR',  CF_USE1|CF_CALL,     cmt='LP <- PC + 2; PC <- Rm31..1||0'),
          idef(0x018F, 'JSRV',  CF_USE1|CF_CALL,     cmt='A subroutine branch instruction involving a toggle between the VLIW and core operation modes. The LTOM bit of the LP register is set to 1.'),

          #### MAJOR OPCODE 2 ####
          idef(0x0200, 'BSETM',   CF_USE1|CF_USE2,     cmt='MemByte(Rm) <- MemByte(Rm) or (1<<imm3)'),
          idef(0x0201, 'BCLRM',   CF_USE1|CF_USE2,     cmt='MemByte(Rm) <- MemByte(Rm) and ~(1<<imm3)'),
          idef(0x0202, 'BNOTM',   CF_USE1|CF_USE2,     cmt='MemByte(Rm) <- MemByte(Rm) xor (1<<imm3)'),
          idef(0x0203, 'BTSTM',   CF_USE1|CF_USE2|CF_USE3,     cmt='MemByte(Rm) <- MemByte(Rm) xor (1<<imm3)'),
          idef(0x0204, 'TAS',   CF_CHG1|CF_USE2,     cmt='temp <- Rm; Rn <- ZeroExt(MemByte(temp)); MemByte(temp) <- 1'),
          idef(0x0205, 'RESERVE_2_5',   0,     cmt='RESERVED OP CODE'),
          idef(0x0206, 'SL1AD3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='R0 <- (Rn<<1) + Rm'),
          idef(0x0207, 'SL2AD3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='R0 <- (Rn<<2) + Rm'),
          idef(0x0208, 'RESERVE_2_8',   0,     cmt='RESERVED OP CODE'),
          idef(0x0209, 'RESERVE_2_9',   0,     cmt='RESERVED OP CODE'),
          idef(0x020A, 'RESERVE_2_10',   0,     cmt='RESERVED OP CODE'),
          idef(0x020B, 'RESERVE_2_11',   0,     cmt='RESERVED OP CODE'),
          idef(0x020C, 'SRL',   CF_CHG1|CF_USE2,     cmt='Rn <- (Unsigned) Rn >> Rm4..0'),
          idef(0x020D, 'SRA',   CF_CHG1|CF_USE2,     cmt='Rn <- (Signed) Rn >> Rm4..0'),
          idef(0x020E, 'SLL',   CF_CHG1|CF_USE2,     cmt='Rn <- (Unsigned) Rn << Rm4..0'),
          idef(0x020F, 'FSFT',   CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- ((Rn||Rm)<<SAR5..0)63..32'),

          #### MAJOR OPCODE 3 ####
          idef(0x0300, 'SWCPI',   CF_USE1|CF_USE2|CF_CHG2,     cmt='MemWord(Rm31..2||00) <- CRn 31..0; Rm<-Rm+4'),
          idef(0x0301, 'LWCPI',   CF_USE1|CF_USE2|CF_CHG1|CF_CHG2,     cmt='CRn <- MemWord(Rm31..2||00); Rm<-Rm+4'),
          idef(0x0302, 'SMCPI',   CF_USE1|CF_USE2|CF_CHG2,     cmt='MemDword(Rm31..3||000) <- CRn; Rm<-Rm+8'),
          idef(0x0303, 'LMCPI',  CF_USE1|CF_USE2|CF_CHG1|CF_CHG2,     cmt='CRn <- MemDword(Rm31..3||000); Rm<-Rm+8'),
          idef(0x0304, 'RESERVE_3_4',   0,     cmt='RESERVED OP CODE'),
          idef(0x0305, 'RESERVE_3_5',   0,     cmt='RESERVED OP CODE'),
          idef(0x0306, 'RESERVE_3_6',   0,     cmt='RESERVED OP CODE'),
          idef(0x0307, 'RESERVE_3_7',   0,     cmt='RESERVED OP CODE'),
          idef(0x0308, 'SWCP',     CF_USE1|CF_USE2,     cmt='MemWord(Rm31..2||00) <- CRn 31..0'),
          idef(0x0309, 'LWCP',     CF_USE1|CF_USE2|CF_CHG1,     cmt='CRn <- MemWord(Rm31..2||00)'),
          idef(0x030A, 'SMCP',     CF_USE1|CF_USE2,     cmt='MemDword(Rm31..3||000) <- CRn'),
          idef(0x030B, 'LMCP',     CF_USE1|CF_USE2|CF_CHG1,     cmt='CRn <- MemDword(Rm31..3||000)'),
          idef(0x030C, 'RESERVE_3_12',   0,     cmt='RESERVED OP CODE'),
          idef(0x030D, 'RESERVE_3_13',   0,     cmt='RESERVED OP CODE'),
          idef(0x030E, 'RESERVE_3_14',   0,     cmt='RESERVED OP CODE'),
          idef(0x030F, 'RESERVE_3_15',   0,     cmt='RESERVED OP CODE'),


          #### MAJOR OPCODE 4 ####
          idef(0x0400, 'ADD3',   CF_CHG1|CF_USE1|CF_USE2|CF_USE3,     cmt='Rn <- SP+ZeroExt((imm7)6..2||00)'),
          idef(0x0402, 'SW',   CF_USE1|CF_USE2|CF_USE3,     cmt='MemWord((ZeroExt((disp7)6..2||00)+SP)31..2||00))<- Rn31..0'), # SW Rn,disp7.align4(SP) << I've modeled this as 3 operands. Hope there wouldnt be an issue
          idef(0x0403, 'LW',   CF_CHG1|CF_USE2|CF_USE3,     cmt='Rn <-MemWord((ZeroExt((disp7)6..2||00)+SP)31..2||00)'),
          idef(0x0412, 'SW',   CF_USE1|CF_USE2|CF_USE3,     cmt='MemWord((ZeroExt((disp7)6..2||00)+TP)31..2||00))<- Rn31..0'),
          idef(0x0413, 'LW',   CF_USE1|CF_CHG1|CF_USE2|CF_USE3,     cmt='Rn <-MemWord((ZeroExt((disp7)6..2||00)+TP)31..2||00)'),
          idef(0x0488, 'LBU',  CF_USE1|CF_CHG1|CF_USE2,     cmt='Rn <-ZeroExt(MemByte((ZeroExt(disp7)+TP)31..0))'),

           #### MAJOR OPCODE 5 ####
          idef(0x0500, 'MOV',   CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- SignExt(imm8)'),

          #### MAJOR OPCODE 6 ####
          idef(0x0600, 'ADD',   CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- Rn + SignExt(imm6)'), #Minor 0 or 4 maps to this
          idef(0x0601, 'SLT3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='if (Rn<ZeroExt(imm5)) R0<-1 else R0<-0(Signed)'),
          idef(0x0602, 'SRL',   CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- (Unsigned) Rn >> imm5'),
          idef(0x0603, 'SRA',   CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- (Signed) Rn >> imm5'),
          idef(0x0605, 'SLTU3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='if (Rn<ZeroExt(imm5)) R0<-1 else R0<-0(Unsigned)'),
          idef(0x0606, 'SLL',   CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- (Unsigned) Rn << imm5'),
          idef(0x0607, 'SLL3',   CF_CHG1|CF_USE2|CF_USE3,     cmt='R0 <- (Unsigned) Rn << imm5'),

          #### MAJOR OPCODE 7 ####
          idef(0x0700, 'DI',   0,     cmt='Disable Interupt'),
          idef(0x0710, 'EI',   0,     cmt='Enable Interupt'),
          idef(0x0711, 'SYNCM',   0,     cmt='Synchronize with Memory'),
          idef(0x0721, 'SYNCCP',   0,     cmt='Synchronize with Coprocessor(CP)'),
          idef(0x0702, 'RET',   0,     cmt='Return'),
          idef(0x0712, 'RETI',   0,     cmt='Return from interrupt'),
          idef(0x0722, 'HALT',   0,     cmt=''),
          idef(0x0732, 'BREAK',   0,     cmt='Generate break interrupts'),
          idef(0x0762, 'SLEEP',   0,     cmt=''),
          idef(0x0713, 'DRET',   0,     cmt='return from the debug exception handler.'),
          idef(0x0733, 'DBREAK',   0,     cmt='Generate debug exceptions'),
          idef(0x0704, 'CACHE',   CF_USE1|CF_USE2,     cmt='Generate debug exceptions'),
          idef(0x0705, 'RESERVE_7_5',  0,     cmt=''),
          idef(0x0706, 'SWI',  CF_USE1,     cmt='Software Interrupt'),
          idef(0x0707, 'RESERVE_7_7',  0,     cmt=''),
          idef(0x0708, 'STC',  CF_CHG1|CF_USE1|CF_USE2,     cmt='ControlReg(imm5) <- Rn'),
          idef(0x070A, 'LDC',  CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <- ControlReg(imm5'),
          idef(0x070C, 'RESERVE_7_12-15',  0,     cmt=''),

          #### MAJOR OPCODE 8 ####
          idef(0x0800, 'SB',  CF_USE1|CF_USE2,     cmt='MemByte((ZeroExt(disp7)+TP)31..0))<- Rn7..0'),
          idef(0x0801, 'SH',  CF_USE1|CF_USE2,     cmt='MemHword((ZeroExt((disp7)6..1||0)+TP)31..1||0))<- Rn15..0'),
          idef(0x0802, 'LB',  CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <-SignExt(MemByte((ZeroExt(disp7)+TP)31..0))'),
          idef(0x0803, 'LH',  CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <-SignExt(MemHword((ZeroExt((disp7)6..1||0)+TP)31..1||0))'),
          idef(0x0804, 'LHU',  CF_CHG1|CF_USE1|CF_USE2,     cmt='Rn <-ZeroExt(MemHword((ZeroExt((disp7)6..1||0)+TP)31..1||0))'),

          #### MAJOR OPCODE 9 ####
          idef(0x0900, 'ADD3',  CF_CHG1|CF_USE1|CF_USE2|CF_USE3,     cmt='Rl <- Rn + Rm'),

           #### MAJOR OPCODE 10 ####
           idef(0x0A00, 'BEQZ',  CF_USE1|CF_USE2,     cmt='if(Rn==0) PC <- PC +SignExt((disp8)7..1||0)'), # TODO: JMP HERE
           idef(0x0A01, 'BNEZ',  CF_USE1|CF_USE2,     cmt='if(Rn!=0) PC <- PC + SignExt((disp8)7..1||0)'), # TODO: JMP HERE

           #### MAJOR OPCODE 11 ####
           idef(0x0B00, 'BRA',  CF_USE1,     cmt='PC <- PC + SignExt((disp12)11..1||0)'), # TODO: JMP HERE
           idef(0x0B01, 'BSR',  CF_USE1,     cmt='LP <- PC + 2; PC <- PC +SignExt((disp12)11..1||0)'), # TODO: JMP HERE
           #idef(0x0A01, 'BNEZ',  CF_USE1|CF_USE2,     cmt='if(Rn!=0) PC <- PC + SignExt((disp8)7..1||0)'), # TODO: JMP HERE

           #And here we being the 32bit instructions. Finally

           #### MAJOR OPCODE 12 ####
           idef(0x0C00, 'ADD3',  CF_CHG1|CF_USE2|CF_USE3,     cmt='Rn <- Rm + SignExt(imm16)'),
           idef(0x0C01, 'MOV',  CF_CHG1|CF_USE2,     cmt='Rn <- SignExt(imm16)'),
           idef(0x0C11, 'MOVU',  CF_CHG1|CF_USE2,     cmt='Rn <- ZeroExt(imm16)'),
           idef(0x0C21, 'MOVH',  CF_CHG1|CF_USE2,     cmt='Rn <- imm16 <<16'),
           idef(0x0C02, 'SLT3',  CF_CHG1|CF_USE2|CF_USE3,     cmt='if (Rm<SignExt(imm16)) Rn<-1 else Rn<-0(Signed)'),
           idef(0x0C03, 'SLTU3',  CF_CHG1|CF_USE2|CF_USE3,     cmt='if (Rm<ZeroExt(imm16)) Rn<-1 else Rn<-0(Unsigned)'),
           idef(0x0C04, 'OR3',  CF_CHG1|CF_USE2|CF_USE3,     cmt='Rn <- Rm or ZeroExt(imm16)'),
           idef(0x0C05, 'AND3',  CF_CHG1|CF_USE2|CF_USE3,     cmt='Rn <- Rm and ZeroExt(imm16)'),
           idef(0x0C06, 'XOR3',  CF_CHG1|CF_USE2|CF_USE3,     cmt='Rn <- Rm xor ZeroExt(imm16)'),
           idef(0x0C07, 'RESERVE_12_7',  0,     cmt=''),
           idef(0x0C08, 'SB',  CF_USE1|CF_USE2,     cmt='MemByte((SignExt(disp16)+Rm)31..0) <- Rn7..0'),
           idef(0x0C09, 'SH',  CF_USE1|CF_USE2,     cmt='MemHword((SignExt(disp16)+Rm)31..1||0) <- Rn15..0'),
           idef(0x0C0A, 'SW',  CF_USE1|CF_USE2,     cmt='MemWord((SignExt(disp16)+Rm)31..2||00) <- Rn31..0'),
           idef(0x0C0B, 'LBU',  CF_CHG1|CF_USE2,     cmt='Rn <- ZeroExt(MemByte((SignExt(disp16)+Rm)31..0)'),
           idef(0x0C0C, 'LB',  CF_CHG1|CF_USE2,     cmt='Rn <-SignExt(MemByte((SignExt(disp16)+Rm)31..0)'),
           idef(0x0C0D, 'LH',  CF_CHG1|CF_USE2,     cmt='Rn <-SignExt(MemHword((SignExt(disp16)+Rm)31..1||0)'),
           idef(0x0C0E, 'LW',  CF_CHG1|CF_USE2,     cmt='Rn <- MemWord((SignExt(disp16)+Rm)31..2||00)'),
           idef(0x0C0F, 'LHU',  CF_CHG1|CF_USE2,     cmt='Rn <- ZeroExt(MemHword((SignExt(disp16)+Rm)31..1||0))'),

           #### MAJOR OPCODE 13 #### Use 13th-17th bits for minor
           idef(0x0D00, 'MOVU',  CF_CHG1|CF_USE2,              cmt='Rn[0-7] <- ZeroExt(imm24)'),
           idef(0x0D04, 'BCPEQ',  CF_USE1|CF_USE2,             cmt='if((cccc xor cp_flag)==0) PC <- PC +SignExt((disp17)16..1||0)'),
           idef(0x0D05, 'BCPNE',  CF_USE1|CF_USE2,     cmt='if((cccc xor cp_flag)!=0) PC <- PC +SignExt((disp17)16..1||0)'),
           idef(0x0D06, 'BCPAT',  CF_USE1|CF_USE2,     cmt='if((cccc and cp_flag)!=0) PC <- PC +SignExt((disp17)16..1||0)'),
           idef(0x0D07, 'BCPAF',  CF_USE1|CF_USE2,     cmt='if((cccc and cp_flag)==0) PC <- PC +SignExt((disp17)16..1||0)'),
           idef(0x0D08, 'JMP',  CF_USE1,                cmt='PC <- PC31..28||0000||(target24)23..1||0'),
           idef(0x0D09, 'BSR',  CF_USE1,                cmt='LP <- PC + 4; PC <- PC +SignExt((disp24)23..1||0)'),
           idef(0x0D0B, 'BSRV',  CF_USE1,                cmt='LP <- (PC + 4)31..1||1;PC <- (PC + SignExt((disp24)23..1||0))31..3||000;PSW.OM <- 1 '),

           #### MAJOR OPCODE 14 ####
           idef(0x0E00, 'BEQI',  CF_USE1|CF_USE2|CF_USE3,              cmt='if(Rn==ZeroExt(imm4)) PC <- PC +SignExt((disp17)16..1||0)'),
           idef(0x0E01, 'BEQ',  CF_USE1|CF_USE2|CF_USE3,              cmt='if(Rn==Rm) PC <- PC +SignExt((disp17)16..1||0)'),
           idef(0x0E04, 'BNEI',  CF_USE1|CF_USE2|CF_USE3,              cmt='if(Rn!=ZeroExt(imm4)) PC <- PC+SignExt((disp17)16..1||0)'),
           idef(0x0E05, 'BNE',  CF_USE1|CF_USE2|CF_USE3,              cmt='if(Rn!=Rm) PC <- PC +SignExt((disp17)16..1||0)'),
           idef(0x0E08, 'BGEI',  CF_USE1|CF_USE2|CF_USE3,              cmt='if(Rn>=ZeroExt(imm4)) PC <- PC +SignExt((disp17)16..1||0)  << (Signed comparison)'),
           idef(0x0E09, 'REPEAT',  CF_USE1|CF_USE2,              cmt='RPB <- pc+4;RPE <- pc+SignExt((disp17)16..1||0));RPC <- Rn; RePeat Rn+1 time till RepeatEnd'),
           idef(0x0E19, 'EREPEAT',  CF_USE1,              cmt='RPB <- pc+4;RPE <- pc+SignExt((disp17)16..1||1));'),
           idef(0x0E0C, 'BLTI',  CF_USE1,              cmt='if(Rn< ZeroExt(imm4)) PC <- PC +SignExt((disp17)16..1||0);(Signed comparison)'),
           idef(0x0E0D, 'RESERVE_15_D',  0,              cmt='REPEAT'),
           idef(0x0E0E, 'SW',  CF_USE1|CF_USE2,              cmt='MemWord(ZeroExt((abs24)23..2||00)) <-Rn31..0'),
           idef(0x0E0F, 'LW',  CF_CHG1|CF_USE2,              cmt='Rn <-MemWord(ZeroExt((abs24)23..2||00))'),

           #### MAJOR OPCODE 15 ####
           #[Major][Minor][sub-minor-1][sub-minor-2]
           #Minor= bits 16-20; sub-minor-1=bit 13-16;  sub-monor-2=bit 1-4
           idef(0xF000, 'DSP',  CF_CHG1|CF_USE1|CF_USE2|CF_USE3,              cmt='Rn <- DSP(Rn,Rm,code16)'), #TODO: There are DSP0 and DSP1
           idef(0xF100, 'LDZ',  CF_CHG1|CF_USE2,                              cmt='Rn <- LeadingZeroDetect(Rm)'),
           idef(0xF102, 'AVE',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- (Rn + Rm + 1)>>1 (Signed)'),
           idef(0xF103, 'ABS',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <-|Rn-Rm|'),
           idef(0xF104, 'MIN',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- Min(Rn, Rm) (Signed)'),
           idef(0xF105, 'MAX',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- Max(Rn, Rm) (Signed)'),
           idef(0xF106, 'MINU',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- Min(Rn, Rm) (Unsigned)'),
           idef(0xF107, 'MAXU',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- Max(Rn, Rm) (Unsigned)'),
           idef(0xF108, 'SADD',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- SaturateAdd(Rn,Rm) (Signed)'),
           idef(0xF109, 'SADDU',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- SaturateAdd(Rn,Rm) (Unsigned)'),
           idef(0xF10A, 'SSUB',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- SaturateSub(Rn,Rm) (Signed)'),
           idef(0xF10B, 'SSUBU',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn <- SaturateSub(Rn,Rm) (Unsigned)'),
           idef(0xF110, 'CLIP',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn(Signed) <- ClipSigned(Rn(Signed),imm5)'),
           idef(0xF111, 'CLIPU',  CF_CHG1|CF_USE1|CF_USE2,                      cmt='Rn(Unsigned) <- ClipUnsigned(Rn(Signed),imm5)'),
           idef(0xF120, 'RESERVER_15_2',   0,                              cmt=''),
           idef(0xF134, 'MADD',   CF_USE1|CF_USE2,                              cmt='HI||LO <- HI||LO + Rn*Rm (Signed)'),
           idef(0xF135, 'MADDU',  CF_USE1|CF_USE2,                               cmt='HI||LO <- HI||LO + Rn*Rm (Unsigned)'),
           idef(0xF136, 'MADDR', CF_CHG1|CF_USE1|CF_USE2,                              cmt='HI||LO <- HI||LO + Rn*Rm; Rn <- LO (Signed)'),
           idef(0xF137, 'MADDRU',CF_CHG1|CF_USE1|CF_USE2,                              cmt='HI||LO <- HI||LO + Rn*Rm; Rn <- LO (Unsigned)'),
           idef(0xF200, 'UCI',CF_CHG1|CF_USE1|CF_USE2|CF_USE3,                              cmt='Rn <- UCI(Rn,Rm,code16)'),
           idef(0xF300, 'RESERVER_15_3',0,                              cmt=''),
           idef(0xF400, 'STCB', CF_USE1|CF_USE2,                              cmt='ControlBus(abs16) <- Rn'),
           idef(0xF401, 'LDCB', CF_USE1|CF_USE2,                              cmt='Rn <- ControlBus(abs16)'),
           idef(0xF500, 'NO_IMPLEMENTED_Mjr_15', 0,                              cmt='??'), # TODO: wth is this co-processor stuff




          # 0x0002
          # 0x0003
          # 0x0004
          # 0x0005
          # 0x0006
          # 0x0007
          #idef(0x0008, 'INT',   0,           cmt='Trigger interrupt A'),
          # idef(0x0009, 'IAG',   CF_CHG1,     cmt='A=IA'),
          # idef(0x000A, 'IAS',   0,           cmt='IA=A'),
          # idef(0x000B, 'RFI',   0,           cmt='Disables interrupt queueing, POP A, POP PC'),
          # idef(0x000C, 'IAQ',   0,           cmt='IF A!=0, interrupt queueing, otherwise triggering'),
          # 0x000D
          # 0x000E
          # 0x000F
          # idef(0x0010, 'HWN',   0,           cmt='A=# of hardware devices'),
          # idef(0x0011, 'HWQ',   0,           cmt='HWID=A+(B<<16), C=Version, MID=X+(Y<<16), Hardware info to A/B/C/X/Y'),
          # idef(0x0012, 'HWI',   0,           cmt='Sends interrupt to hardware device A'),
          # 0x0013
          # 0x0014
          # 0x0015
          # 0x0016
          # 0x0017
          # 0x0018
          # 0x0019
          # 0x001A
          # 0x001B
          # 0x001C
          # 0x001D
          # 0x001E
          # 0x001F
          # idef(0x0100, 'SET',   CF_CHG1,     cmt='B=A'),
          # idef(0x0200, 'ADD',   CF_CHG1,     cmt='B=B+A, EX=0x01 on overflow'),
          # idef(0x0300, 'SUB',   CF_CHG1,     cmt='B=B-A, EX=0xFFFF on underflow'),
          # idef(0x0400, 'MUL',   CF_CHG1,     cmt='B=B*A, EX=High 16 overflow bits'),
          # idef(0x0500, 'MLI',   CF_CHG1,     cmt='A=B*A, Signed, EX=High 16 overflow bits'),
          # idef(0x0600, 'DIV',   CF_CHG1,     cmt='A=B/A, EX=0 if either A or B == 0, EX=(b<<16)/a'),
          # idef(0x0700, 'DVI',   CF_CHG1,     cmt='B=B/A, Signed, EX=0 if either A or B == 0, EX=(b<<16)/a'),
          # idef(0x0800, 'MOD',   CF_CHG1,     cmt='B=B%A, if a==0, b=0'),
          # idef(0x0900, 'MDI',   CF_CHG1,     cmt='B=B%A, Signed, if a==0, b=0'),
          # idef(0x0A00, 'AND',   CF_CHG1,     cmt='B=B&A'),
          # idef(0x0B00, 'BOR',   CF_CHG1,     cmt='B=B|A'),
          # idef(0x0C00, 'XOR',   CF_CHG1,     cmt='B=B^A'),
          # idef(0x0D00, 'SHR',   CF_CHG1,     cmt='B=B>>A, EX=(B<<16)>>A'),
          # idef(0x0E00, 'ASR',   CF_CHG1,     cmt='B=B>>A, Signed, EX=(B<<16)>>A'),
          # idef(0x0F00, 'SHL',   CF_CHG1,     cmt='B=B<<A, EX=(B<<A)>>16'),
          # idef(0x1000, 'IFB',   0, con=True, cmt='(B&A)!=0'),
          # idef(0x1100, 'IFC',   0, con=True, cmt='(B&A)==0'),
          # idef(0x1200, 'IFE',   0, con=True, cmt='B==A'),
          # idef(0x1300, 'IFN',   0, con=True, cmt='B!=A'),
          # idef(0x1400, 'IFG',   0, con=True, cmt='B>A'),
          # idef(0x1500, 'IFA',   0, con=True, cmt='B>A, Signed'),
          # idef(0x1600, 'IFL',   0, con=True, cmt='B<A'),
          # idef(0x1700, 'IFU',   0, con=True, cmt='B<A, Signed'),
          # # 0x1800
          # # 0x1900
          # idef(0x1A00, 'ADX',   0,           cmt='B=B+A+EX, EX=1 on overflow'),
          # idef(0x1B00, 'SBX',   0,           cmt='B=B-A+EX, EX=0xFFFF on underflow'),
          # # 0x1C00
          # # 0x1D00
          # idef(0x1E00, 'STI',   0,           cmt='B=A, ++I, ++J'),
          # idef(0x1F00, 'STD',   0,           cmt='B=A, --I, --J'),
          ]


      # Now create an instruction table compatible with IDA processor module requirements
      self.otable = {}
      self.ntable = {}
      Instructions = []
      i = 0
      for x in self.itable:
        self.otable[x.op] = x
        self.ntable[x.name] = x
        x.itype = i
        Instructions.append(x.ToDict())
        setattr(self, 'itype_' + x.name, i)
        i += 1

      # icode of the last instruction + 1
      self.instruc_end = len(Instructions) + 1

      # Array of instructions
      self.instruc = Instructions

      # Icode of return instruction. It is ok to give any of possible return
      # instructions
      self.icode_return = 0

################# END INSTRUCTION ###################
    # ----------------------------------------------------------------------
    # Handle Opcodes
    #
    # ----------------------------------------------------------------------
    def decode_Mj0(self,Opcode): #decode major opcode zero
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b1111
        if(Op_Major)!=0:
            print "ERROR in Major Op Code =0"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])


        if Op_Minor==0: # MOV Rn,RM
            cmd.itype=self.itable.index(self.otable[0x0000]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==1:
            #cmd.itype=0x0001
            cmd.itype=self.itable.index(self.otable[0x0001])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==2:
            #cmd.itype=0x0002
            cmd.itype=self.itable.index(self.otable[0x0002])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>8 & 0b1111
            cmd.Op3.type=o_reg
            cmd.Op3.reg=Opcode>>4 & 0b1111
        elif Op_Minor==3:
            #cmd.itype=0x0003
            cmd.itype=self.itable.index(self.otable[0x0003])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>8 & 0b1111
            cmd.Op3.type=o_reg
            cmd.Op3.reg=Opcode>>4 & 0b1111
        elif Op_Minor==4:
            cmd.itype=self.itable.index(self.otable[0x0004])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==5:
            cmd.itype=self.itable.index(self.otable[0x0005])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>8 & 0b1111
            cmd.Op3.type=o_reg
            cmd.Op3.reg=Opcode>>4 & 0b1111
        elif Op_Minor==6:
            cmd.itype=self.itable.index(self.otable[0x0006])
        elif Op_Minor==7:
            cmd.itype=self.itable.index(self.otable[0x0007])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>8 & 0b1111
            cmd.Op3.type=o_reg
            cmd.Op3.reg=Opcode>>4 & 0b1111
        elif Op_Minor==8:
            cmd.itype=self.itable.index(self.otable[0x0008])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==9:
            cmd.itype=self.itable.index(self.otable[0x0009]) #SH
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==10:
            cmd.itype=self.itable.index(self.otable[0x000A]) #SW
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==11:
            cmd.itype=self.itable.index(self.otable[0x000B]) #LBU
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==12:
            cmd.itype=self.itable.index(self.otable[0x000C]) #LB
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==13:
            cmd.itype=self.itable.index(self.otable[0x000D]) #LH
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==14:
            cmd.itype=self.itable.index(self.otable[0x000E]) #LW
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==15:
            cmd.itype=self.itable.index(self.otable[0x000F]) #LHU
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode >>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])


    def decode_Mj1(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b1111
        if(Op_Major)!=1:
            print "ERROR in Major Op Code =1"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if Op_Minor==0: # OR Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0100]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==1: # AND Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0101]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==2: # XOR Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0102]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==3: # NOR Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0103]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==4: # MUL Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0104]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==5: # MULU Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0105]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==6: # MULR Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0106]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==7: # MULRU Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0107]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==8: # DIV Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0108]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==9: # DIVU Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0109]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==10: # RI
            cmd.itype=self.itable.index(self.otable[0x010A]) #Look Up itable to find appropriate itype
        elif Op_Minor==11: # RI
            cmd.itype=self.itable.index(self.otable[0x010B]) #Look Up itable to find appropriate itype
        elif Op_Minor==12: # SSARB disp2(Rm) ###!!!!
            cmd.itype=self.itable.index(self.otable[0x010C]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>4 & 0b1111
        elif Op_Minor==13:  #EXTB* Rn
            if (Opcode>>4 & 0b1111) ==0b0000:
                cmd.itype=self.itable.index(self.otable[0x010D]) #EXTB Rn
            elif (Opcode>>4 & 0b1111) ==0b0010:
                cmd.itype=self.itable.index(self.otable[0x012D]) #EXTH Rn
            elif (Opcode>>4 & 0b1111) ==0b1000:
                cmd.itype=self.itable.index(self.otable[0x018D]) #EXTUB Rn
            elif (Opcode>>4 & 0b1111) ==0b1010:
                cmd.itype=self.itable.index(self.otable[0x01AD]) #EXTUH Rn
            else:
                cmd.itype=self.itable.index(self.otable[0xabad1dea])
                return
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
        elif Op_Minor==14: #  JMP
            cmd.itype=self.itable.index(self.otable[0x010E]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>4 & 0b1111
        elif Op_Minor==15: # JSR*
            if (Opcode>>8 & 0b1111) ==0b0000:   #JSR
                cmd.itype=self.itable.index(self.otable[0x010F]) #Look Up itable to find appropriate itype
            elif (Opcode>>8 & 0b1111) ==0b1000:  #JSRV
                cmd.itype=self.itable.index(self.otable[0x018F]) #EXTH Rn
            else:
                cmd.itype=self.itable.index(self.otable[0xabad1dea])
                return
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>4 & 0b1111
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj2(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b1111
        if(Op_Major)!=2:
            print "ERROR in Major Op Code =2"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if Op_Minor==0: # BSETM (Rm), imm3
            cmd.itype=self.itable.index(self.otable[0x0200]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_phrase
            cmd.Op1.phrase=Opcode>>4 & 0b1111
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode>>8 & 0b0111
        elif Op_Minor==1: #BCLRM (Rn), imm3
            cmd.itype=self.itable.index(self.otable[0x0201]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_phrase
            cmd.Op1.phrase=Opcode>>4 & 0b1111
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode>>8 & 0b0111
        elif Op_Minor==2: #BNOTM (Rm), imm3
            cmd.itype=self.itable.index(self.otable[0x0202]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_phrase
            cmd.Op1.phrase=Opcode>>4 & 0b1111
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode>>8 & 0b0111
        elif Op_Minor==3: #BTSTM R0,(Rm), imm3
            cmd.itype=self.itable.index(self.otable[0x0203]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
            cmd.Op3.type=o_imm
            cmd.Op3.value=Opcode>>8 & 0b0111
        elif Op_Minor==4: #TAS Rn,(Rm)
            cmd.itype=self.itable.index(self.otable[0x0204]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==5:#RI
            cmd.itype=self.itable.index(self.otable[0x0205]) #Look Up itable to find appropriate itype
        elif Op_Minor==6:#SL1AD3 R0,Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0206]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>8 & 0b1111
            cmd.Op3.type=o_reg
            cmd.Op3.reg=Opcode>>4 & 0b1111
        elif Op_Minor==7: #SL2AD3 R0,Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0207]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>8 & 0b1111
            cmd.Op3.type=o_reg
            cmd.Op3.reg=Opcode>>4 & 0b1111
        elif Op_Minor==8:#RI
            cmd.itype=self.itable.index(self.otable[0x0208]) #Look Up itable to find appropriate itype
        elif Op_Minor==9:#RI
            cmd.itype=self.itable.index(self.otable[0x0209]) #Look Up itable to find appropriate itype
        elif Op_Minor==10:#RI
            cmd.itype=self.itable.index(self.otable[0x020A]) #Look Up itable to find appropriate itype
        elif Op_Minor==11:#RI
            cmd.itype=self.itable.index(self.otable[0x020B]) #Look Up itable to find appropriate itype
        elif Op_Minor==12:#SRL Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x020C]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==13:#SRA Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x020D]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==14: #  SLL Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x020E]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        elif Op_Minor==15: #FSFT Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x020F]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=Opcode>>8 & 0b1111
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode>>4 & 0b1111
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj3(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b1111
        if(Op_Major)!=3:
            print "ERROR in Major Op Code =3"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if Op_Minor==0: # SWCPI CRn,(Rm+)
            cmd.itype=self.itable.index(self.otable[0x0300]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b0111
        elif Op_Minor==1: #LWCPI CRn,(Rm+)
            cmd.itype=self.itable.index(self.otable[0x0301]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b0111
        elif Op_Minor==2: #SMCPI CRn,(Rm+)
            cmd.itype=self.itable.index(self.otable[0x0302]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b0111
        elif Op_Minor==3: #LMCPI CRn,(Rm+)
            cmd.itype=self.itable.index(self.otable[0x0303]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b0111
        elif Op_Minor==4:#RI
            cmd.itype=self.itable.index(self.otable[0x0304]) #Look Up itable to find appropriate itype
        elif Op_Minor==5:#RI
            cmd.itype=self.itable.index(self.otable[0x0305]) #Look Up itable to find appropriate itype
        elif Op_Minor==6:#RI
            cmd.itype=self.itable.index(self.otable[0x0306]) #Look Up itable to find appropriate itype
        elif Op_Minor==7:#RI
            cmd.itype=self.itable.index(self.otable[0x0307]) #Look Up itable to find appropriate itype
        elif Op_Minor==8: #SWCP CRn,(Rm)
            cmd.itype=self.itable.index(self.otable[0x0308]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==9: # LWCP CRn,(Rm)
            cmd.itype=self.itable.index(self.otable[0x0309]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg= (Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==10: # SMCP CRn,(Rm)
            cmd.itype=self.itable.index(self.otable[0x030A]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg= (Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif Op_Minor==11: # LMCP CRn,(Rm)
            cmd.itype=self.itable.index(self.otable[0x030B]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg= (Opcode>>8 & 0b1111) + 30 # Coprocessor general-purpose registers starts from Index 30 in register table
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=Opcode>>4 & 0b1111
        elif 12<=Op_Minor <=15:#RI
            cmd.itype=self.itable.index(self.otable[0x0300 + Op_Minor]) #Look Up itable to find appropriate itype
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj4(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b0011
        if(Op_Major)!=4:
            print "ERROR in Major Op Code =4"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if (Opcode & 0b0100100010000000) ==0b0100100010000000: # LBU Rn[0-7],disp7(TP)  << # Special Major Minor opcode
            cmd.itype=self.itable.index(self.otable[0x0488]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b0111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode & 0b01111111
            cmd.Op3.type=o_reg
            cmd.Op3.reg=13
            return

        if Op_Minor==0: # ADD3 Rn,SP,imm7.align4
            cmd.itype=self.itable.index(self.otable[0x0400]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=15
            cmd.Op3.type=o_imm
            cmd.Op3.value=(Opcode & 0b1111100)
        elif Op_Minor==2: # SW Rn,disp7.align4(SP)
            if (Opcode & 0b10000000)==0: # SW Rn,disp7.align4(SP)
                cmd.itype=self.itable.index(self.otable[0x0402])
                cmd.Op1.type=o_reg
                cmd.Op1.reg=(Opcode>>8 & 0b1111)
                cmd.Op2.type=o_imm
                cmd.Op2.value= Opcode & 0b1111100
                cmd.Op3.type=o_reg
                cmd.Op3.reg=15
            else:       # SW Rn[0-7],disp7.align4(TP)
                cmd.itype=self.itable.index(self.otable[0x0412])
                cmd.Op1.type=o_reg
                cmd.Op1.reg=(Opcode>>8 & 0b0111)
                cmd.Op2.type=o_imm
                cmd.Op2.value= Opcode & 0b1111100
                cmd.Op3.type=o_reg
                cmd.Op3.reg=13
        elif Op_Minor==3:
            if (Opcode & 0b10000000)==0: #LW Rn,disp7.align4(SP)
                cmd.itype=self.itable.index(self.otable[0x0403])
                cmd.Op1.type=o_reg
                cmd.Op1.reg=(Opcode>>8 & 0b1111)
                cmd.Op2.type=o_imm
                cmd.Op2.value= Opcode & 0b1111100
                cmd.Op3.type=o_reg
                cmd.Op3.reg=15
            else:   # LW Rn[0-7],disp7.align4(TP)
                cmd.itype=self.itable.index(self.otable[0x0413])
                cmd.Op1.type=o_reg
                cmd.Op1.reg=(Opcode>>8 & 0b0111)
                cmd.Op2.type=o_imm
                cmd.Op2.value= Opcode & 0b1111100
                cmd.Op3.type=o_reg
                cmd.Op3.reg=13
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj5(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        #Op_Minor= Opcode & 0b1111
        if(Op_Major)!=5:
            print "ERROR in Major Op Code =5"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return
        #MOV Rn,imm8
        cmd.itype=self.itable.index(self.otable[0x0500]) #Look Up itable to find appropriate itype
        cmd.Op1.type=o_reg
        cmd.Op1.reg=Opcode>>8 & 0b1111
        cmd.Op2.type=o_imm
        cmd.Op2.value=Opcode & 0b11111111

    def decode_Mj6(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b111 # only 3 bits matter
        if(Op_Major)!=6:
            print "ERROR in Major Op Code =6"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if (Op_Minor==0) or (Op_Minor==4): # ADD Rn,imm6
            cmd.itype=self.itable.index(self.otable[0x0600]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=(Opcode>>2 & 0b111111)
        elif Op_Minor==1: # SLT3 R0,Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x0601]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=(Opcode>>8 & 0b1111)
            cmd.Op3.type=o_imm
            cmd.Op3.value=(Opcode>>3 & 0b11111)
        elif Op_Minor==2: # SRL Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x0602]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=(Opcode>>3 & 0b11111)
        elif Op_Minor==3: # SRA Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x0603]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=(Opcode>>3 & 0b11111)
        elif Op_Minor==5: # SLTU3 R0,Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x0605]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=(Opcode>>8 & 0b1111)
            cmd.Op3.type=o_imm
            cmd.Op3.value=(Opcode>>3 & 0b11111)
        elif Op_Minor==6: # SLL Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x0606]) # Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=(Opcode>>3 & 0b11111)
        elif Op_Minor==7: # SLL3 R0,Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x0607]) # Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=0
            cmd.Op2.type=o_reg
            cmd.Op2.reg=(Opcode>>8 & 0b1111)
            cmd.Op3.type=o_imm
            cmd.Op3.value=(Opcode>>3 & 0b11111)
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj7(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b1111
        if(Op_Major)!=7:
            print "ERROR in Major Op Code =7"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return
        if Opcode in[0x7000, 0x7010, 0x7011, 0x7021, 0x7002, 0x7012, 0x7022,0x7032,0x7062,0x7013,0x7033]:
            opcode_tmp= (Opcode & 0xFF)|0x0700
            cmd.itype=self.itable.index(self.otable[opcode_tmp]) #Look Up itable to find appropriate itype
            return

        if (Op_Minor==4): # CACHE imm4, (Rm)
            cmd.itype=self.itable.index(self.otable[0x0704]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_imm
            cmd.Op1.value=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_phrase
            cmd.Op2.phrase=(Opcode>>4 & 0b1111)
        elif (Op_Minor==5): # RESERVE_7_5
            cmd.itype=self.itable.index(self.otable[0x0705]) #Look Up itable to find appropriate itype
        elif (Op_Minor==6): # SWI imm2
            cmd.itype=self.itable.index(self.otable[0x0706]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_imm
            cmd.Op1.value=(Opcode>>4 & 0b0011)
        elif (Op_Minor==7): # RESERVE_7_7
            cmd.itype=self.itable.index(self.otable[0x0707]) #Look Up itable to find appropriate itype
        elif (Op_Minor==8) or (Op_Minor==9) : # STC Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x0708]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=100+((Opcode& 0b1)<<4 | (Opcode>>4 & 0b1111))
        elif (Op_Minor==10) or (Op_Minor==11) : # LDC Rn,imm5
            cmd.itype=self.itable.index(self.otable[0x070A]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=100+((Opcode& 0b1)<<4 | (Opcode>>4 & 0b1111))
        elif 11<Op_Minor<16:
            cmd.itype=self.itable.index(self.otable[0x070C]) #Look Up itable to find appropriate itype
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj8(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b100010000001 #only 03 very specific bits are important
        if(Op_Major)!=8:
            print "ERROR in Major Op Code =8"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if (Op_Minor==0) or (Op_Minor==1): # SB Rn[0-7],disp7(TP)
            cmd.itype=self.itable.index(self.otable[0x0800]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b111)
            cmd.Op2.type=o_displ
            cmd.Op2.phrase=13
            cmd.Op2.addr=(Opcode & 0b1111111)
        elif (Op_Minor==0b10000000): # SH Rn[0-7],disp7.align2(TP)
            cmd.itype=self.itable.index(self.otable[0x0801]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b111)
            cmd.Op2.type=o_displ
            cmd.Op2.phrase=13
            cmd.Op2.addr=(Opcode & 0b1111111)
        elif (Op_Minor==0b100000000000) or (Op_Minor==0b100000000001): # LB Rn[0-7],disp7(TP)
            cmd.itype=self.itable.index(self.otable[0x0802]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b111)
            cmd.Op2.type=o_displ
            cmd.Op2.phrase=13
            cmd.Op2.addr=(Opcode & 0b1111111)
        elif (Op_Minor==0b100010000000): # LH Rn[0-7],disp7(TP)
            cmd.itype=self.itable.index(self.otable[0x0803]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b111)
            cmd.Op2.type=o_displ
            cmd.Op2.phrase=13
            cmd.Op2.addr=(Opcode & 0b1111111)
        elif (Op_Minor==0b100010000001): # LHU Rn[0-7],disp7(TP)
            cmd.itype=self.itable.index(self.otable[0x0804]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode>>8 & 0b111)
            cmd.Op2.type=o_displ
            cmd.Op2.phrase=13
            cmd.Op2.addr=(Opcode & 0b1111111)
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj9(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b100010000001 #only 03 very specific bits are important
        if(Op_Major)!=9:
            print "ERROR in Major Op Code =9"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if (Opcode & 0b1111)==0b1111: # ADD3 Rl,Rn,Rm
            cmd.itype=self.itable.index(self.otable[0x0900]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=1
            cmd.Op2.type=o_reg
            cmd.Op2.reg=(Opcode>>8 & 0b1111)
            cmd.Op3.type=o_reg
            cmd.Op3.reg=(Opcode>>4 & 0b1111)
        else:
            cmd.itype=self.itable.index(self.otable[0xabad1dea])

    def decode_Mj10(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b1 #only 1 very specific bits is important
        if(Op_Major)!=10:
            print "ERROR in Major Op Code =10"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return
        cmd.itype=self.itable.index(self.otable[0x0A00+Op_Minor]) #Look Up itable to find appropriate itype
        cmd.Op1.type=o_reg
        cmd.Op1.reg=(Opcode>>8 & 0b1111)
        cmd.Op2.type=o_displ
        cmd.Op2.addr=(Opcode & 0b11111110) # last bit is not important
        cmd.Op2.phrase=100

    def decode_Mj11(self,Opcode):
        Op_Major= (Opcode>>12) & 0b1111
        Op_Minor= Opcode & 0b1 #only 1 bit is important
        if(Op_Major)!=11:
            print "ERROR in Major Op Code =11"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if (Op_Minor)==0: # BRA disp12.align2
            cmd.itype=self.itable.index(self.otable[0x0B00]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_imm
            temp_value= SIGNEXT((Opcode & 0b111111111110),12) & 0b1111111111111111 #sign extend. Todo first instance of signed extend implementation. need to implement everywhere
            temp_value= struct.pack('H',temp_value)
            temp_value= struct.unpack('h',temp_value)
            cmd.Op1.value=temp_value[0] #temp_value[0] would be signed short integer @ this point
            cmd.Op1.dtyp=dt_word
        else:       #BSR disp12.align2
            cmd.itype=self.itable.index(self.otable[0x0B01]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_imm
            temp_value= SIGNEXT((Opcode & 0b111111111110),12) & 0b1111111111111111 #sign extend. Todo first instance of signed extend implementation. need to implement everywhere
            temp_value= struct.pack('H',temp_value)
            temp_value= struct.unpack('h',temp_value)
            cmd.Op1.value=temp_value[0]
            cmd.Op1.dtyp=dt_word #TODO: Set dtyp for operands
    def decode_Mj12(self,Opcode_1,Opcode_2):
        Op_Major= (Opcode_1>>12) & 0b1111
        Op_Minor= Opcode_1 & 0b1111 #only 4 bits are important
        if(Op_Major)!=12:
            print "ERROR in Major Op Code =12"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

        if (Op_Minor)==0: # ADD3 Rn,Rm,imm16
            cmd.itype=self.itable.index(self.otable[0x0C00]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 &0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=(Opcode_1>>4 & 0b1111)
            cmd.Op3.type=o_imm
            cmd.Op3.value=Opcode_2 # TODO need to figure out if we want to call SIGN EXTEND HERE
            cmd.Op3.dtyp=dt_word
        elif (Op_Minor==1): # MOV|U|H Rn,imm16
            if (Opcode_1>>4&0b1111)==0:  #MOV
                cmd.itype=self.itable.index(self.otable[0x0C01]) #Look Up itable to find appropriate itype
            elif (Opcode_1>>4&0b1111)==1:  #MOVU
                cmd.itype=self.itable.index(self.otable[0x0C11]) #Look Up itable to find appropriate itype
            elif (Opcode_1>>4&0b1111)==2:  #MOVH
                cmd.itype=self.itable.index(self.otable[0x0C21]) #Look Up itable to find appropriate itype
            else:
                cmd.itype=self.itable.index(self.otable[0xabad1dea])
                return

            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 &0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode_2
            cmd.Op2.dtyp=dt_word

        elif (2<=Op_Minor<=6): # SLTU3 to XOR3 Rn,Rm,imm16
            cmd.itype=self.itable.index(self.otable[0x0C00+ Op_Minor]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 &0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=(Opcode_1>>4 & 0b1111)
            cmd.Op3.type=o_imm
            cmd.Op3.value=Opcode_2
            cmd.Op3.dtyp=dt_word
        elif (Op_Minor==7): # SLTU3 Rn,Rm,imm16
            cmd.itype=self.itable.index(self.otable[0x0C07]) #Look Up itable to find appropriate itype

        elif (8<=Op_Minor<=15): # SB to LHU Rn,disp16(Rm)
            cmd.itype=self.itable.index(self.otable[0x0C00+ Op_Minor]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 &0b1111)
            cmd.Op2.type=o_displ
            cmd.Op2.phrase=(Opcode_1>>4 & 0b1111)
            cmd.Op2.addr=SIGNEXT_32bits(Opcode_2,16) #TODO << This should be the gold standard of Sign_ETEND. All signed extension should be done in ana() rather then out() because certain opcode does zero_extend. EG compare SB Rn,disp16(Rm)  V.S LB Rn[0-7],disp7(TP)
            cmd.Op2.dtyp=dt_word

        else:  #should never even reach here
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

    def decode_Mj13(self,Opcode_1,Opcode_2):
        Op_Major= (Opcode_1>>12) & 0b1111
        Op_Minor= Opcode_1 & 0b1111 #only 4 bits are important
        if(Op_Major)!=13:
            print "ERROR in Major Op Code =13"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return
        if (Opcode_1>>8 & 0b1000)==0:#MOVU Rn[0-7],imm24
            cmd.itype=self.itable.index(self.otable[0x0D00]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b0111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode_2<<8 | Opcode_1 & 0b11111111
            cmd.Op2.dtyp=dt_dword
            return

        elif (4<=Op_Minor<=7): # BCPEQ, BCPNE, BCPAT, BCPAF
            cmd.itype=self.itable.index(self.otable[0x0D00 + Op_Minor]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_imm
            cmd.Op1.value=(Opcode_1 >>4 &0b1111)
            cmd.Op1.dtyp=dt_byte

            cmd.Op2.type=o_imm
            cmd.Op2.value=cmd.ea + (SIGNEXT_32bits((Opcode_2<<1),17))  #TODO: verify against objdump. Op2 should be PC + o_imm. Need to handle all those PC<- PC + o_imm
            cmd.Op2.dtyp=dt_dword

        elif (Op_Minor==8): #JMP target24
            cmd.itype=self.itable.index(self.otable[0x0D08]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_mem
            temp=((Opcode_1 >>4)&0b01111111)<<1
            cmd.Op1.addr=(Opcode_2 << 8) | temp
            cmd.Op1.dtyp=dt_dword

        elif (Op_Minor==9) or (Op_Minor==0xB): #BSR  or BSRV
            cmd.itype=self.itable.index(self.otable[0x0D09]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_mem
            temp=((Opcode_1 >>4)&0b01111111)<<1
            print "###OP_9####%x --%x --%x" % (temp,(Opcode_2 << 8),(Opcode_2 << 8) | temp)
            cmd.Op1.addr= cmd.ea + SIGNEXT_32bits(((Opcode_2 << 8) | temp),24)
            cmd.Op1.dtyp=dt_dword

        else:  #should never even reach here
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

    def decode_Mj14(self,Opcode_1,Opcode_2):
        Op_Major= (Opcode_1>>12) & 0b1111
        Op_Minor= Opcode_1 & 0b1111 #only 4 bits are important
        if(Op_Major)!=14:
            print "ERROR in Major Op Code =14"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return
        if Op_Minor == 0:#BEQI Rn,imm4,disp17
            cmd.itype=self.itable.index(self.otable[0x0E00]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode_1>>4 & 0b1111
            cmd.Op2.dtyp=dt_byte
            cmd.Op3.type=o_near
            cmd.Op3.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1),17)
            return

        elif Op_Minor==1: # BEQ Rn,Rm,disp17
            cmd.itype=self.itable.index(self.otable[0x0E01]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode_1>>4 & 0b1111
            cmd.Op3.type=o_near
            cmd.Op3.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1),17)
            return

        elif Op_Minor == 4:#BNEI Rn,imm4,disp17
            cmd.itype=self.itable.index(self.otable[0x0E04]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode_1>>4 & 0b1111
            cmd.Op2.dtyp=dt_byte
            cmd.Op3.type=o_near
            cmd.Op3.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1),17)
            return

        elif Op_Minor == 5:#BNE Rn,Rm,disp17
            cmd.itype=self.itable.index(self.otable[0x0E05]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode_1>>4 & 0b1111
            cmd.Op3.type=o_near
            cmd.Op3.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1),17)
            return

        elif Op_Minor == 8:#BGEI Rn,imm4,disp17
            cmd.itype=self.itable.index(self.otable[0x0E08]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode_1>>4 & 0b1111
            cmd.Op2.dtyp=dt_byte
            cmd.Op3.type=o_near
            cmd.Op3.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1),17)
            return

        elif Op_Minor == 9:
            if (Opcode_1>>4) &0b1 ==0:
                cmd.itype=self.itable.index(self.otable[0x0E09]) #REPEAT Rn,disp17
                cmd.Op1.type=o_reg
                cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
                cmd.Op2.type=o_near
                cmd.Op2.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1),17)
            else:
                cmd.itype=self.itable.index(self.otable[0x0E19]) #EREPEAT disp17.align2
                cmd.Op1.type=o_near
                cmd.Op1.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1) ,17) #TODO: Manual says Bit 0th is set to 1 so as to differentiate between EREPEAT and REPEAT but objdump doesnt. I think the 1 bit is only for tracking internally.
            return

        elif Op_Minor == 12:#BLTI Rn,imm4,disp17
            cmd.itype=self.itable.index(self.otable[0x0E0C]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=Opcode_1>>4 & 0b1111
            cmd.Op2.dtyp=dt_byte
            cmd.Op3.type=o_near
            cmd.Op3.addr=cmd.ea + SIGNEXT_32bits((Opcode_2<<1),17)
            return
        elif Op_Minor == 13:#RESERVE_15_D
            cmd.itype=self.itable.index(self.otable[0x0E0D]) #Look Up itable to find appropriate itype
            return

        elif (Op_Minor & 0b0011)== 0b10:#SW Rn,(abs24)
            cmd.itype=self.itable.index(self.otable[0x0E0E]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_mem
            cmd.Op2.addr=SIGNEXT_32bits((Opcode_2<<8) | ( Opcode_1 & 0b11111100),24) #TODO need to test this complex case
            return

        elif (Op_Minor & 0b0011)== 0b11:#LW Rn,(abs24)
            cmd.itype=self.itable.index(self.otable[0x0E0F]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_mem
            cmd.Op2.addr=SIGNEXT_32bits((Opcode_2<<8) | ( Opcode_1 & 0b11111100),24) #TODO need to test this complex case
            return
        else:  #should never even reach here
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return

    def decode_Mj15(self,Opcode_1,Opcode_2):
        Op_Major= (Opcode_1>>12) & 0b1111
        Op_Minor= Opcode_1 & 0b1111 # only 4 bits are important
        if(Op_Major)!=15:
            print "ERROR in Major Op Code =15"
            cmd.itype=self.itable.index(self.otable[0xabad1dea])
            return
        if Op_Minor == 0:#DSP Rn,Rm,code16
            cmd.itype=self.itable.index(self.otable[0xF000]) #Look Up itable to find appropriate itype
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode_1>>4 & 0b1111
            cmd.Op3.type=o_imm
            cmd.Op3.value=Opcode_2
            cmd.Op3.dtyp = dt_word
        elif Op_Minor == 1 and Opcode_2 in [0,2,3,4,5,6,7,8,9,10,11]:
            if Opcode_2 & 0b1111==0: #LDZ Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF100])
            elif Opcode_2 & 0b1111==2:#AVE Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF102])
            elif Opcode_2 & 0b1111==3:#ABS Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF103])
            elif Opcode_2 & 0b1111==4:#MIN Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF104])
            elif Opcode_2 & 0b1111==5:#MAX Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF105])
            elif Opcode_2 & 0b1111==6:#MINU Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF106])
            elif Opcode_2 & 0b1111==7:#MAXU Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF107])
            elif Opcode_2 & 0b1111==8:#SADD Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF108])
            elif Opcode_2 & 0b1111==9:#SADDU Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF109])
            elif Opcode_2 & 0b1111==10:#SSUB Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF10A])
            elif Opcode_2 & 0b1111==11:#SSUBU Rn,Rm
                cmd.itype=self.itable.index(self.otable[0xF10B])
            else:
                cmd.itype=self.itable.index(self.otable[0xabad1dea])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode_1>>4 & 0b1111
        elif Op_Minor == 1 and (Opcode_2 & 0b1111111100000111) in [0x1000,0x1001]: # CLIP|CLIPU Rn,imm5
            cmd.itype=self.itable.index(self.otable[0xF110 + (Opcode_2 & 0b1)])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.value=(Opcode_2 >>3 & 0b11111)
            cmd.Op2.dtyp=dt_byte
        elif Op_Minor == 1 and Opcode_2 in range(0x2000,0x2FFF+1):
            cmd.itype=self.itable.index(self.otable[0xF120]) #RESERVER_15_2
        elif Op_Minor == 1 and Opcode_2 in [0x3004,0x3005,0x3006,0x3007]:#MADD,MADDU,MADDR,MADDRU
            cmd.itype=self.itable.index(self.otable[0xF130 + (Opcode_2 & 0b1111)])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode_1>>4 & 0b1111
        elif Op_Minor == 2 : #UCI
            cmd.itype=self.itable.index(self.otable[0xF200])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_reg
            cmd.Op2.reg=Opcode_1>>4 & 0b1111
            cmd.Op3.type=o_imm
            cmd.Op3.dtyp=dt_word
            cmd.Op3.value=Opcode_2
        elif Op_Minor == 3 : #RESERVER_15_3
            cmd.itype=self.itable.index(self.otable[0xF300])
        elif Op_Minor == 4 and (Opcode_1>>4 & 0b1111) in [0,1]: #STCB||LDCB Rn,abs16
            cmd.itype=self.itable.index(self.otable[0xF400 + (Opcode_1>>4 & 0b1111)])
            cmd.Op1.type=o_reg
            cmd.Op1.reg=(Opcode_1 >>8 & 0b1111)
            cmd.Op2.type=o_imm
            cmd.Op2.dtyp=dt_word
            cmd.Op2.value=Opcode_2
        else:
            cmd.itype=self.itable.index(self.otable[0xF500]) #NO_IMPLEMENTED_Mjr_15
            return



    def __init__(self):

        self.regNames = [
            # General purpose registers
            "R0", # aka R0
            "R1",
            "R2",
            "R3",
            "R4",
            "R5",
            "R6",
            "R7",
            "R8",
            "R9",
            "R10",
            "R11",
            "R12",
            "TP", # aka  R13
            "GP", # aka R14
            "SP", # aka R15 (Stack  pointer)
            # VM registers
            "FLAGS", # 0
            "IP",    # 1
            "VM2",
            "VM3",
            "VM4",
            "VM5",
            "VM6",
            "VM7",
            # Fake segment registers
            "CS",
            "DS",
            "PlaceHolder","PlaceHolder","PlaceHolder","PlaceHolder",
            ####START OF COPROCESSOR REGISTERS  30#####
            "CR0",
            "CR1",
            "CR2",
            "CR3",
            "CR4",
            "CR5",
            "CR6",
            "CR7",
            "CR8",
            "CR9",
            "CR10",
            "CR11",
            "CR12",
            "CR13",
            "CR14",
            "CR15",
            "CCRN"
        ]
        while len(self.regNames) !=100:
            self.regNames.append("PlaceHolder")
        self.regNames[100:]=[
            # Control/Special registers starts from 100
            "PC",
            "LP",
            "SAR",
            "RESERVE_CONTROL_REG3",
            "$rpb",
            "$rpe",
            "$rpc",
            "$hi",
            "$lo",
            "RESERVE_CONTROL_REG9",
            "RESERVE_CONTROL_REG10",
            "RESERVE_CONTROL_REG11",
            "$mb0",
            "$me0",
            "$mb1",
            "$me1",
            "$psw",
            "$id",
            "$tmp",
            "$epc",
            "$exc",
            "$cfg",
            "RESERVE_CONTROL_REG22",
            "$npc",
            "$dbg",
            "$depc",
            "$opt",
            "$rcfg",
            "$ccfg",
            "RESERVE_CONTROL_REG29",
            "RESERVE_CONTROL_REG30",
            "RESERVE_CONTROL_REG31",
            "RESERVE_CONTROL_REG32",


        ]

            # Create the ireg_XXXX constants
        for i in xrange(len(self.regNames)):
            setattr(self, 'ireg_' + self.regNames[i], i)

            # Segment register information (use virtual CS and DS registers if your
            # processor doesn't have segment registers):
        self.regFirstSreg = self.ireg_CS
        self.regLastSreg  = self.ireg_DS

        # number of CS register
        self.regCodeSreg = self.ireg_CS

        # number of DS register
        self.regDataSreg = self.ireg_DS
        self.instruc_end = 11
        self.init_instructions()

    id = 0x8000  # IDP id ( Numbers above 0x8000 are reserved for the third-party modules)
    flag = PR_SEGS | PR_DEFSEG32 | PR_USE32 | PRN_HEX | PR_RNAMESOK | PR_NO_SEGMOVE
    # Number of bits in a byte for code segments (usually 8)
    # IDA supports values up to 32 bits
    cnbits = 8

    # Number of bits in a byte for non-code segments (usually 8)
    # IDA supports values up to 32 bits
    dnbits = 8

    # short processor names
    # Each name should be shorter than 9 characters
    psnames = ['MeP']

    # long processor names
    # No restriction on name lengthes.
    plnames = ['Crappy MeP']


    # size of a segment register in bytes
    segreg_size = 0

    # Array of typical code start sequences (optional)
    # codestart = ['\x60\x00']  # 60 00 xx xx: MOVqw         SP, SP-delta

    # Array of 'return' instruction opcodes (optional)
    # retcodes = ['\x04\x00']   # 04 00: RET

    # You should define 2 virtual segment registers for CS and DS.
    # Let's call them rVcs and rVds.

    # icode of the first instruction
    instruc_start = 0
    tbyte_size = 0

    assembler = {
        # flag
        'flag': ASH_HEXF3 | AS_UNEQU | AS_COLON | ASB_BINF4 | AS_N2CHR,

        # user defined flags (local only for IDP)
        # you may define and use your own bits
        'uflag': 0,

        # Assembler name (displayed in menus)
        'name': "libNex MeP Assembler",

        # org directive
        'origin': "org",

        # end directive
        'end': "end",

        # comment sstring (see also cmnt2)
        'cmnt': ";",

        # ASCII string delimiter
        'ascsep': "\"",

        # ASCII char constant delimiter
        'accsep': "'",

        # ASCII special chars (they can't appear in character and ascii constants)
        'esccodes': "\"'",

        #
        #      Data representation (db,dw,...):
        #
        # ASCII string directive
        'a_ascii': "db",

        # byte directive
        'a_byte': "db",

        # word directive
        'a_word': "dw",

        # remove if not allowed
        'a_dword': "dd",

        # remove if not allowed
        'a_qword': "dq",

        # remove if not allowed
        'a_oword': "xmmword",

        # float;  4bytes; remove if not allowed
        'a_float': "dd",

        # double; 8bytes; NULL if not allowed
        'a_double': "dq",

        # long double;    NULL if not allowed
        'a_tbyte': "dt",

        # array keyword. the following
        # sequences may appear:
        #      #h - header
        #      #d - size
        #      #v - value
        #      #s(b,w,l,q,f,d,o) - size specifiers
        #                        for byte,word,
        #                            dword,qword,
        #                            float,double,oword
        'a_dups': "#d dup(#v)",

        # uninitialized data directive (should include '%s' for the size of data)
        'a_bss': "%s dup ?",

        # 'seg ' prefix (example: push seg seg001)
        'a_seg': "seg",

        # current IP (instruction pointer) symbol in assembler
        'a_curip': "$",

        # "public" name keyword. NULL-gen default, ""-do not generate
        'a_public': "public",

        # "weak"   name keyword. NULL-gen default, ""-do not generate
        'a_weak': "weak",

        # "extrn"  name keyword
        'a_extrn': "extrn",

        # "comm" (communal variable)
        'a_comdef': "",

        # "align" keyword
        'a_align': "align",

        # Left and right braces used in complex expressions
        'lbrace': "(",
        'rbrace': ")",

        # %  mod     assembler time operation
        'a_mod': "%",

        # &  bit and assembler time operation
        'a_band': "&",

        # |  bit or  assembler time operation
        'a_bor': "|",

        # ^  bit xor assembler time operation
        'a_xor': "^",

        # ~  bit not assembler time operation
        'a_bnot': "~",

        # << shift left assembler time operation
        'a_shl': "<<",

        # >> shift right assembler time operation
        'a_shr': ">>",

        # size of type (format string)
        'a_sizeof_fmt': "size %s",
    }  # Assembler

    # ----------------------------------------------------------------------
    # Processor module callbacks
    #
    # ----------------------------------------------------------------------
    def get_frame_retsize(self, func_ea):
        """
        Get size of function return address in bytes
        MEP doesn't use stack but the link register
        """
        return 0

    def notify_get_autocmt(self):
        """
        Get instruction comment. 'cmd' describes the instruction in question
        @return: None or the comment string
        """
        return "myComment" + cmd

    def is_align_insn(self, ea):
        """
        No Idea what this does
        Is the instruction created only for alignment purposes?
        Returns: number of bytes in the instruction
        """
        return 0

    def notify_newfile(self, filename):
        pass


    def notify_oldfile(self, filename):
        pass

    def header(self):
        """function to produce start of disassembled text"""
        MakeLine("MYHEADER!!!; natural unit size: %d bits")


    # def notify_may_be_func(self, state):
    #     """
    #     can a function start here?
    #     the instruction is in 'cmd'
    #       arg: state -- autoanalysis phase
    #         state == 0: creating functions
    #               == 1: creating chunks
    #       returns: probability 0..100
    #     """
    #     if is_reg(self.cmd.Op1, self.ireg_SP) and cmd.Op2.type == o_displ and\
    #         cmd.Op2.phrase == self.ireg_SP and (cmd.Op2.specval & self.FLo_INDIRECT) == 0:
    #         # mov SP, SP+delta
    #         if SIGNEXT(self.cmd.Op2.addr, self.PTRSZ*8) < 0:
    #             return 100
    #         else:
    #             return 0
    #     return 10

    def ana(self):
        """
        Decodes an instruction into the C global variable 'cmd'
        """
        print "in ANA"
        Op1 = ua_next_word() #ua_next_byte()
        print "%x" %Op1
        #b = ua_next_word() #ua_next_byte()
        #print "%x" %b
        if Op1 & (0b1111<<12) ==0:
            self.decode_Mj0(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==1:
            print "Major Opcode=1"
            self.decode_Mj1(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==2:
            print "Major Opcode=2"
            self.decode_Mj2(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==3:
            print "Major Opcode=3"
            self.decode_Mj3(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==4:
            print "Major Opcode=4"
            self.decode_Mj4(Op1)
            return
        elif (Op1>>12) & 0b1111 ==5:
            print "Major Opcode=5"
            self.decode_Mj5(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==6:
            print "Major Opcode=6"
            self.decode_Mj6(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==7:
            print "Major Opcode=7"
            self.decode_Mj7(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==8:
            print "Major Opcode=8"
            self.decode_Mj8(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==9:
            print "Major Opcode=9"
            self.decode_Mj9(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==10:
            print "Major Opcode=10"
            self.decode_Mj10(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==11:
            print "Major Opcode=11"
            self.decode_Mj11(Op1)
            return 2
        elif (Op1>>12) & 0b1111 ==12:
            print "Major Opcode=12"
            #self.decode_Mj11(b)
            print "First word= %x" %Op1
            Op2 = ua_next_word() #ua_next_byte()
            print "2nd Word = %x" %Op2
            self.decode_Mj12(Op1,Op2)
            return 4
        elif (Op1>>12) & 0b1111 ==13:
            print "Major Opcode=13"
            #self.decode_Mj11(b)
            Op2 = ua_next_word() #ua_next_byte()
            self.decode_Mj13(Op1,Op2)
            return 4
        elif (Op1>>12) & 0b1111 ==14:
            print "Major Opcode=14"
            #self.decode_Mj11(b)
            Op2 = ua_next_word() #ua_next_byte()
            self.decode_Mj14(Op1,Op2)
            return 4
        elif (Op1>>12) & 0b1111 ==15:
            print "Major Opcode=15"
            #self.decode_Mj11(b)
            Op2 = ua_next_word() #ua_next_byte()
            self.decode_Mj15(Op1,Op2)
            return 4
        else:
            print "unknown Major opcode=%x" % (Op1 & (0b1111<<12))
        pprint.pprint(cmd)
        cmd.itype =int(Op1)
        #cmd.size=2
        cmd.Op1.type=o_imm
        cmd.Op1.dtype=dt_qword


        print "size = %d" % cmd.size
        print "end ANA"
        return 2
        #return 0

    def emu(self):
        """
        Emulate instruction, create cross-references, plan to analyze
        subsequent instructions, modify flags etc. Upon entrance to this function
        all information about the instruction is in 'cmd' structure.
        If zero is returned, the kernel will delete the instruction.
        """
        print "in emu()"
        pprint.pprint(cmd)
        Feature = cmd.get_canon_feature()
        #print "OP1 = %x" % cmd.Op1.value
        #ua_add_cref(0, cmd.ea + cmd.size, fl_F)
        if Feature & CF_JUMP:
            print "Here's a Jump %x" % cmd.ea
            QueueMark(Q_jumps, cmd.ea)
            return 1
        if cmd.itype ==0x12:
            cmd.Op1.type=o_reg# optype_t: 0_void, 0_reg egg
            cmd.Op1.reg=1  #value depends on .type. For o_mem = addr for o_imm = value
            cmd.Op1.dtyp=dt_dword # Type of Operand Value

            #ua_add_cref(0,0x10,fl_JF)
            #ua_add_cref(0,0x12,fl_JF)

            ua_add_cref(0,0x10,fl_CN)
        if cmd.itype ==0x6666:
            ua_add_cref(0,0x20,fl_JN) #FL_JN and #FL_JF no diff it seems
            ua_add_cref(0,0x26,fl_JF)

        if cmd.itype !=0:
            ua_add_cref(0, cmd.ea + cmd.size, fl_F)

        return 1

    def out(self):
        """
        # Generate text representation of an instruction in 'cmd' structure.
        # This function shouldn't change the database, flags or anything else.
        # All these actions should be performed only by u_emu() function.
        # Init output buffer
        # WTF is dbgprint
        """
        buf = idaapi.init_output_buffer(1024)
        OutMnem(2)
        print "in out() cmd type=%x",cmd.itype
        #OutMnem(2)
        for x in range(0,5):
            if cmd.Operands[x].type == o_void:
                break
            out_one_operand(x)#arg = OPerand #

        pprint.pprint(cmd)

        #out_colored_register_line()

        #out_one_operand(1)
        #out_line("myOutLine",COLOR_INSN)
        term_output_buffer()
        cvar.gl_comm = 1
        MakeLine(buf)

    def outop(self, op):
        """
        Generate text representation of an instructon operand.
        This function shouldn't change the database, flags or anything else.
        All these actions should be performed only by u_emu() function.
        The output text is placed in the output buffer initialized with init_output_buffer()
        This function uses out_...() functions from ua.hpp to generate the operand text

        self.cmd will be initialized by ana().

        Overrides:
          idaapi.processor_t

        Returns:
          1-ok, 0-operand is hidden.
        """
        print "in outop()"
        #out_symbol('[mySymbol]')
        #out_register(self.regNames[op.reg])
        #out_long(0x123,16)
        if op.n!=0:     #if it's not 1st operand, we'll probably need a ","
            out_symbol(",")

        if op.type==o_reg:
            out_register(self.regNames[op.reg])
        elif op.type==o_mem:
            OutValue(op,OOFS_NEEDSIGN|OOFW_32|OOF_ADDR)
        elif op.type in [o_mem,o_imm]:
            OutValue(op)
        elif op.type in [o_far,o_near]:
            OutValue(op,OOFW_32|OOF_ADDR)
        elif op.type==o_phrase:
            out_symbol("[")
            out_register(self.regNames[op.phrase])
            out_symbol("]")
        elif op.type==o_displ:
            out_symbol("[") #TODO: May need to remove [ ] and add it only of its  derefence. EG BNEZ BEQZ does not need []
            out_register(self.regNames[op.phrase])

            OutValue(op,OOFS_NEEDSIGN|OOFW_32|OOF_ADDR) # in a o_displ, the value should always be 32bits i think


            out_symbol("]")
        else:
            out_long(0xabad1dea,16)


        return True


def PROCESSOR_ENTRY():
    return mep_processor_t()